use rusqlite::{params_from_iter, Connection};
use std::time::{Instant};
use sea_query::*;
sea_query::sea_query_driver_rusqlite!();
use sea_query_driver_rusqlite::{RusqliteValues};

enum S {
    S(String)
}

impl Iden for S {
    fn unquoted(&self, s: &mut dyn std::fmt::Write) {
        write!(
            s,
            "{}",
            match self {
                Self::S(s) => s
            }
        )
        .unwrap();
    }
}

fn table_create() -> String {
    r#"
        CREATE TABLE test_table (
            col1 TEXT,
            col2 TEXT,
            col3 TEXT,
            col4 TEXT,
            col5 TEXT,
            col6 TEXT,
            col7 TEXT,
            col8 TEXT,
            col9 TEXT,
            col10 TEXT
    )"#.to_string()
}

fn output_timings(start: &Instant, number_of_entries: &u128) {
    let msecs = start.elapsed().as_millis();
    let inserts_per_second = if msecs == 0 {0} else {
        if msecs > 0 {
            (number_of_entries / msecs) * 1000
        } else {
            0
        }
    };

    let secs = format!("{}.{} sec", msecs / 1000, msecs % 1000);
    println!("{} entries inserted in ~{}. ~{} inserts/sec",
                number_of_entries, secs, inserts_per_second );
}

fn setup(conn: &rusqlite::Connection) {
    conn.execute("DROP TABLE test_table", []).ok();
    conn.execute(&table_create(), []).ok();
}

fn main() {
    let number_of_entries = 1_000_000;


    // -----------------------------------------
    // --- SQLite without prepared statement ---
    // -----------------------------------------
    println!("Using SQLite directly");
    let mut data = Vec::new();
    for _ in 1..number_of_entries {
        data.push(vec![
            "1".to_string(),
            "2".to_string(),
            "3".to_string(),
            "4".to_string(),
            "5".to_string(),
            "6".to_string(),
            "7".to_string(),
            "8".to_string(),
            "9".to_string(),
            "10".to_string(),
        ]);
    }

    let conn = Connection::open_in_memory().unwrap();
    setup(&conn);
    let start = Instant::now();
    conn.execute("BEGIN TRANSACTION;", []).unwrap();
    for values in data {
        let sql = format!(
            "INSERT INTO test_table VALUES ({}, {}, {}, {}, {}, {}, {}, {}, {}, {})",
            values[0], values[1], values[2], values[3],
            values[4], values[5], values[6], values[7],
            values[8], values[9]
        );
        conn.execute(&sql, []).unwrap();
    }
    conn.execute("COMMIT;", []).unwrap();
    output_timings(&start, &number_of_entries);


    // ---------------------------------
    // --- SQLite prepared statement ---
    // ---------------------------------
    println!("Using SQLite prepared statement");
    let mut data = Vec::new();
    for _ in 1..number_of_entries {
        data.push(vec![
            "1".to_string(),
            "2".to_string(),
            "3".to_string(),
            "4".to_string(),
            "5".to_string(),
            "6".to_string(),
            "7".to_string(),
            "8".to_string(),
            "9".to_string(),
            "10".to_string(),
        ]);
    }
    setup(&conn);
    let mut prepared_statement = conn.prepare("INSERT INTO test_table VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)").unwrap();

    let start = Instant::now();
    conn.execute("BEGIN TRANSACTION;", []).unwrap();
    for values in data {
        prepared_statement.execute(params_from_iter(values)).unwrap();
    }
    conn.execute("COMMIT;", []).unwrap();
    output_timings(&start, &number_of_entries);


    // ----------------------------
    // --- sea_query individual ---
    // ----------------------------
    println!("Using sea_query as in examples");
    setup(&conn);
    let mut data = Vec::new();
    for _ in 1..number_of_entries {
        data.push(vec![
            "1".into(),
            "2".into(),
            "3".into(),
            "4".into(),
            "5".into(),
            "6".into(),
            "7".into(),
            "8".into(),
            "9".into(),
            "10".into(),
        ]);
    }
    conn.execute("BEGIN TRANSACTION;", []).unwrap();
    for d in data {
        let (sql, values) = sea_query::Query::insert()
            .into_table(S::S("test_table".to_string()))
            .columns(vec![
                S::S("col1".to_string()),
                S::S("col2".to_string()),
                S::S("col3".to_string()),
                S::S("col4".to_string()),
                S::S("col5".to_string()),
                S::S("col6".to_string()),
                S::S("col7".to_string()),
                S::S("col8".to_string()),
                S::S("col9".to_string()),
                S::S("col10".to_string())
            ])
            .values_panic(d)
            .build(SqliteQueryBuilder);

        conn.execute(sql.as_str(), RusqliteValues::from(values).as_params().as_slice()).unwrap();

    }
    conn.execute("COMMIT;", []).unwrap();
    output_timings(&start, &number_of_entries);


    // ----------------------------
    // --- sea_query with guard ---
    // ----------------------------
    println!("Using sea_query with guard");
    setup(&conn);
    let mut data = Vec::new();
    for _ in 1..number_of_entries {
        data.push(vec![
            "1".into(),
            "2".into(),
            "3".into(),
            "4".into(),
            "5".into(),
            "6".into(),
            "7".into(),
            "8".into(),
            "9".into(),
            "10".into(),
        ]);
    }

    let mut q = sea_query::Query::insert()
        .into_table(S::S("test_table".to_string()))
        .columns(vec![
            S::S("col1".to_string()),
            S::S("col2".to_string()),
            S::S("col3".to_string()),
            S::S("col4".to_string()),
            S::S("col5".to_string()),
            S::S("col6".to_string()),
            S::S("col7".to_string()),
            S::S("col8".to_string()),
            S::S("col9".to_string()),
            S::S("col10".to_string())
        ])
        .to_owned();
    let mut guard = false;
    for d in &data {
        if guard == true {
            continue
        }
        q.values_panic(d.clone());
        guard = true;
    }
    let (sql, _) = q.build(SqliteQueryBuilder);
    let data_2 = data.into_iter().map(|d| {
        let vals = Values(d);
        RusqliteValues::from(vals)
    });

    let start = Instant::now();
    conn.execute("BEGIN TRANSACTION;", []).unwrap();
    for rq_vals in data_2 {
        conn.execute(&sql,  rq_vals.as_params().as_slice()).unwrap();

    }
    conn.execute("COMMIT;", []).unwrap();
    output_timings(&start, &number_of_entries);


    // ---------------------------------------------------
    // --- sea_query with guard and prepared statement ---
    // ---------------------------------------------------
    println!("Using sea_query with guard and prepared statement");
    setup(&conn);
    let mut data = Vec::new();
    for _ in 1..number_of_entries {
        data.push(vec![
            "1".into(),
            "2".into(),
            "3".into(),
            "4".into(),
            "5".into(),
            "6".into(),
            "7".into(),
            "8".into(),
            "9".into(),
            "10".into(),
        ]);
    }

    let mut q = sea_query::Query::insert()
        .into_table(S::S("test_table".to_string()))
        .columns(vec![
            S::S("col1".to_string()),
            S::S("col2".to_string()),
            S::S("col3".to_string()),
            S::S("col4".to_string()),
            S::S("col5".to_string()),
            S::S("col6".to_string()),
            S::S("col7".to_string()),
            S::S("col8".to_string()),
            S::S("col9".to_string()),
            S::S("col10".to_string()),

        ])
        .to_owned();
    q.values_panic(data[0].clone());
    let (sql, _) = q.build(SqliteQueryBuilder);
    let mut prepared_stmt = conn.prepare(&sql).unwrap();
    let start = Instant::now();
    conn.execute("BEGIN TRANSACTION;", []).unwrap();
    for d in data {
        let rq_vals = RusqliteValues::from(Values(d));
        prepared_stmt.execute(rq_vals.as_params().as_slice()).unwrap();

    }
    conn.execute("COMMIT;", []).unwrap();
    output_timings(&start, &number_of_entries);


    // -------------------------------------------------------
    // --- sea_query - process everything before executing ---
    // -------------------------------------------------------
    println!("Using sea_query, everything processed before sql execution");
    setup(&conn);
    let mut data = Vec::new();
    for _ in 1..number_of_entries {
        data.push(vec![
            "1".into(),
            "2".into(),
            "3".into(),
            "4".into(),
            "5".into(),
            "6".into(),
            "7".into(),
            "8".into(),
            "9".into(),
            "10".into(),
        ]);
    }

    let mut q = sea_query::Query::insert()
        .into_table(S::S("test_table".to_string()))
        .columns(vec![
            S::S("col1".to_string()),
            S::S("col2".to_string()),
            S::S("col3".to_string()),
            S::S("col4".to_string()),
            S::S("col5".to_string()),
            S::S("col6".to_string()),
            S::S("col7".to_string()),
            S::S("col8".to_string()),
            S::S("col9".to_string()),
            S::S("col10".to_string()),
        ])
        .to_owned();
    q.values_panic(data[0].clone());
    let (sql, _) = q.build(SqliteQueryBuilder);
    let mut prepared_stmt = conn.prepare(&sql).unwrap();
    let prepared_vals = data.into_iter().map(|d| {
        RusqliteValues::from(Values(d))
    });

    let start = Instant::now();
    conn.execute("BEGIN TRANSACTION;", []).unwrap();
    for v in prepared_vals {
        prepared_stmt.execute(v.as_params().as_slice()).unwrap();

    }
    conn.execute("COMMIT;", []).unwrap();
    output_timings(&start, &number_of_entries);
}
